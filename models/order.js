const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    orderName: {
        type: String,
        trim: true,
        required: true
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
})

module.exports = mongoose.model('order', orderSchema)
