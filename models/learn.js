const mongoose = require('mongoose')

const learnSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    salary: {
        type: Number,
        required: true
    },
    dob: {
        type: String,
        trim: true,
        required: true
    },
    day: {
        type: String,
        trim: true,
        required: true
    },
})

module.exports = mongoose.model('learnIndex', learnSchema)
