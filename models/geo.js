const mongoose = require('mongoose')

const geoSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    location: {
        coordinates: [
            {
                type: Number,
                required: true
            }
        ],
        type: {
            type: String,
            enum: ['Point'],
            required: true
        }
    }
}, {
    timestamps: true
})




geoSchema.index({ location: '2dsphere' })

module.exports = mongoose.model('geo', geoSchema)