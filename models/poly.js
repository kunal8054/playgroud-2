const mongoose = require('mongoose');

const polySchema = new mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    location: {
        coordinates: [
            [[
                {
                    type: Number,
                    required: true
                }
            ]

            ]
        ], type:
        {
            type: String,
            enum: ['Polygon'],
            required: true
        }


    }
})

polySchema.index({ location: "2dsphere" })

module.exports = mongoose.model('poly', polySchema);

