const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    }
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

userSchema.virtual('orders', {
    ref: 'order',
    localField: '_id',
    foreignField: 'userId'
})








module.exports = mongoose.model('user', userSchema)