//     https://www.cs.usfca.edu/~galles/visualization/BTree.html

// function run() {


//     for (var i = 0; i < 10000; i++) {
//         var age = (Math.floor((Math.random() * 100) + 1))
//         var salary = (Math.floor((Math.random() * 100000) + 1))

//         console.log('Age ' + age + ' Salary ' + salary)

//     }

// }
// run()

const mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/learn')
const Learn = require('./models/learn')




function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

// for (var i = 0; i < 100; i++) {
//     console.log(makeid(5));

// }


function insertData() {

    var name = makeid(5)


    var array = []


    for (var i = 0; i < 100000; i++) {
        array.push(makeid(5))
    }

    // for (var i = 0; i < 100000; i++) {
    //     console.log(array[i])
    // }

    console.log(array.length);


    for (var i = 0; i < 100000; i++) {
        var age = (Math.floor((Math.random() * 100) + 1))
        var salary = (Math.floor((Math.random() * 100000) + 1))
        console.log('name: ' + array[i] + ' \tage' + age + '\t \tSalary ' + salary);
    }



}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomDate() {
    // aprox nr of days since 1970 untill 2000: 30years * 365 days
    var nr_days1 = 30 * 365;
    // aprox nr of days since 1950 untill 1970: 20years * 365 days
    var nr_days2 = -20 * 365;

    // milliseconds in one day
    var one_day = 1000 * 60 * 60 * 24

    // get a random number of days passed between 1950 and 2000
    var days = getRandomInt(nr_days2, nr_days1);

    return new Date(days * one_day)
}


// for (var i = 0; i < 10; i++) {
//     const date = getRandomDate().toString()
//     console.log(date.slice(0,15));
// }










// insertData()

async function run() {

    var array = []


    for (var i = 0; i < 100000; i++) {
        array.push(makeid(5))
    }
    var date = []

    for (var i = 0; i < 100000; i++) {
        date.push(getRandomDate().toString())
        // console.log(date.slice(0, 15));
    }

    for (var i = 0; i < 100000; i++) {
        const learn = await Learn.create({
            name: array[i],
            age: (Math.floor((Math.random() * 100) + 1)),
            salary: (Math.floor((Math.random() * 100000) + 1)),
            dob: date[i].slice(4, 15),
            day: date[i].slice(0, 4)

        })

        // console.log(learn)
    }
    console.log('Done');

}

run()
