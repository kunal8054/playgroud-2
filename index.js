const express = require('express');
const app = express();
const mongoose = require('mongoose');

const port = 3000

app.use(express.json())
mongoose.connect('mongodb://127.0.0.1:27017/virtuals')


const User = require('./models/users')
const Order = require('./models/order')
const learnIndex = require('./models/learn')
const Geo = require('./models/geo')
const Poly = require('./models/poly')



app.get('/geo', async (req, res) => {

    // const geo = await Geo.create({
    //     location: {
    //         coordinates: [-73.8803827, 40.7643124],
    //         type: 'Point'
    //     },
    //     name: "Hell's' Kitchen"
    // })

    // const geo = await Geo.create(
    //     {
    //         name: "34D-1",
    //         location: { type: "Point", coordinates: [76.75722432144329, 30.719288066445635] },
    //     }

    // )
    // const geo1 = await Geo.create(
    //     {
    //         name: "34D-2",
    //         location: { type: "Point", coordinates: [76.76211155521744, 30.713473751046276] },
    //     }

    // )
    // const geo2 = await Geo.create(
    //     {
    //         name: "34D-3",
    //         location: { type: "Point", coordinates: [76.76778864495509, 30.717038774327833] },
    //     }

    // )
    // const geo3 = await Geo.create(
    //     {
    //         name: "34D-4",
    //         location: { type: "Point", coordinates: [76.76275331318779, 30.722852874776354] },
    //     }

    // )

    // res.send({ geo, geo1, geo2, geo3 })

    // const options = {
    //     location: {
    //         $geoWithin: {
    //             $centerSphere: [[77.4520708, 28.848447], 15 / 3963.2]
    //         }
    //     }
    // }

    const options = {
        location: {
            $near: {
                $geometry: { type: "Point", coordinates: [76.76275331318779, 30.722852874776354] },
                $minDistance: 100,
                $maxDistance: 1000,
            }
        }
    }

    const data = await Geo.find(options)
    res.send(data)



})








app.get('/', async (req, res) => {


    try {
        // const poly = await Poly.create(
        //     {
        //         name: "A",
        //         location: {
        //             type: "Polygon", coordinates: [
        //                 [
        //                     [0, 1],
        //                     [1, 2],
        //                     [2, 1],
        //                     [0, 1]


        //                 ]
        //             ]
        //         },
        //     }
        // )

        // res.send(poly)


        // const output = await poly.find({
        //     location: {
        //         $geoWithin: {
        //             $geometry: {
        //                 "type": "Polygon",
        //                 "coordinates": [[
        //                     [0, 1],
        //                     [1, 2],
        //                     [2, 1],
        //                     [0, 1]
        //                 ]]
        //             }
        //         }
        //     }
        // })

        // const output = await Poly.find(
        //     {
        //         location: {
        //             $geoIntersects: {
        //                 $geometry: {
        //                     "type": "Polygon",
        //                     "coordinates": [[
        //                         [1, 1],
        //                         [1, 1.5],
        //                         [1.5, 1.5],
        //                         [1, 1]
        //                     ]]
        //                 }
        //             }
        //         }
        //     }
        // )

        const output = await Poly.find(
            {
                location: {
                    $geoIntersects: {
                        $geometry: {
                            "type": "Point",
                            "coordinates": [1.5, 1.5]
                        }
                    }
                }
            }
        )



        res.send(output)









    } catch (error) {
        res.send(error.message)
    }



})




































app.get('/data', async (req, res) => {


    // const user = await User.create({
    //     name: 'Ankit'
    // })

    // const user1 = await User.create({
    //     name: 'Aanchal'
    // })

    // res.send(user)

    // const order1 = await Order.create({
    //     orderName: 'Order-1',
    //     userId: '6204e6914c932f8b22838f8f'
    // })
    // const order2 = await Order.create({
    //     orderName: 'Order-2',
    //     userId: '6204e6914c932f8b22838f8f'
    // })
    // const order3 = await Order.create({
    //     orderName: 'Order-3',
    //     userId: '6204e65d86e60f18ebc0cae9'
    // })

    // res.send({ order1, order2, order3 })


})


app.get('/virtuals', async (req, res) => {

    // const user = await Order.find({}).populate('userId')
    const user = await User.find({}).populate('orders')


    res.send(user)

})



app.get('/', async (req, res) => {

    const learn1 = await learnIndex.find({ 'name': 'eWT4A' })

    console.log(learn1, 'here');
    res.send("working");
    // res.send(learn)

})








app.get('/regex', async (req, res) => {

    res.send('Working')

})











app.listen(port, (error) => {
    if (error) {
        return console.log('0Error starting server \n' + error)
    }
    console.log('Server is up and running on port: ' + port)
})

